import java.util.SortedSet;
import java.util.TreeSet;

public class Test {
    public static void printTree(TreeSet<String> tree) {
        System.out.println("===== Tree =====");
        for (String s : tree){
            System.out.printf("%s ", s);
        }
        System.out.println();
    }

    public static void main(String[] args) {
        //Создание пустого дерева
        TreeSet<String> tree = new TreeSet<>();

        //Добавление элементов в дерево
        tree.add("AAA");
        tree.add("BBB");

        //Элементы выводятся в сортированном (лексикографическом) порядке
        //Классы должны быть Comparable
        printTree(tree);

        //Удаление существующего элементов - возвращает true
        System.out.println(tree.remove("AAA"));

        //Удаление несуществующего элемента - возвращает false
        System.out.println(tree.remove("CCC"));
        printTree(tree);

        tree.add("XXX");
        tree.add("YYY");
        tree.add("ZZZ");
        tree.add("000");

        printTree(tree);

        //Проверка наличия элемента в дереве
        System.out.println(tree.contains("XXX"));

        //Выводит наименьший элемент больший или равный указанного
        System.out.println("ceiling BBA = " + tree.ceiling("BBA"));
        //null если нет такого элемента
        System.out.println(tree.ceiling("QQQ"));

        //Выводит наибольший элемент меньший или равный указанного
        System.out.println("floor BBC = " + tree.floor("BBC"));
        //null если нет такого элемента
        System.out.println(tree.floor("QQQ"));

        System.out.println("===== Subtree =====");

        //Взятие подмножества элементов >= XXX и < ZZZ
        SortedSet<String> subtree = tree.subSet("XXX", "ZZZ");
        for (String s : subtree) {
            System.out.printf("%s ", s);
        }
        System.out.println();
    }
}
